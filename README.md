# Challenge 3 Binar Academy

Challenge kali ini bertujuan untuk mengatur responsive web dari desain yang diberikan supaya bisa diakses melalui beberapa device seperti mobile, tablet, dan dekstop

## Teknologi yang digunakan

Di challenge ini saya menggunakan 2 framework dan beberapa bahasa yaitu :

- Bootstrap 5
- Owl Carousel
- HTML
- CSS
- Javascript

di challenge ini masih seputar web responsive jadi bahasa yang digunakan hanya HTML, CSS dan Javascript.

Bootstrap yang saya gunakan adalah bootstrap v 5. di challenge ini saya menggunakan bootstrap untuk penambahan class ditiap elemen, mengatur spacing tiap element, dan menggunakan component yang disediakan oleh bootstrap 5 seperti offcanvas, burger menu, dan accordion.

Owl Carousel sendiri saya gunakan untuk membuat slider pada component testimonial, dibagian ini ada sedikit saya custom terutama dibagian arrow button dan ukuran setiap item.

## Cara menjalankan project

project bisa dijalankan dengan cloning repository ini atau download file .zip, lalu bisa buka file main.html di browser masing - masing
